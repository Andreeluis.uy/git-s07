Microsoft Windows [Version 10.0.19044.2604]
(c) Microsoft Corporation. All rights reserved.

C:\Users\Francis Kyle>mysql -u root
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 10
Server version: 5.5.5-10.4.24-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> USE blog_db
Database changed
mysql> INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com","passwordA", "2021-01-01 01:00:00");
Query OK, 1 row affected (0.01 sec)

mysql> INSERT INTO users (email, password, datetime_created) VALUES ("juandelecruz@gmail.com","passwordB", "2021-01-01 02:00:00");
Query OK, 1 row affected (0.01 sec)

mysql> INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com","passwordC", "2021-01-01 03:00:00");
Query OK, 1 row affected (0.00 sec)

mysql> INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com","passwordD", "2021-01-01 04:00:00");
Query OK, 1 row affected (0.01 sec)

mysql> INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com","passwordE", "2021-01-01 05:00:00");
Query OK, 1 row affected (0.00 sec)

mysql> SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelecruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
|  5 | johndoe@gmail.com       | passwordE | 2021-01-01 05:00:00 |
+----+-------------------------+-----------+---------------------+
5 rows in set (0.00 sec)

mysql> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World", "2021-01-02 01:00:00");
ERROR 1054 (42S22): Unknown column 'title' in 'field list'
mysql> SHOW TABLE posts;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'posts' at line 1
mysql> DESCRIBE posts;
+-----------------+--------------+------+-----+---------+----------------+
| Field           | Type         | Null | Key | Default | Extra          |
+-----------------+--------------+------+-----+---------+----------------+
| id              | int(11)      | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)      | NO   | MUL | NULL    |                |
| content         | varchar(500) | YES  |     | NULL    |                |
| datetime_posted | datetime     | YES  |     | NULL    |                |
+-----------------+--------------+------+-----+---------+----------------+
4 rows in set (0.01 sec)

mysql> ALTER TABLE posts ADD COLUMN title VARCHAR(500) AFTER author_id;
Query OK, 0 rows affected (0.01 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> DESCRIBE posts;
+-----------------+--------------+------+-----+---------+----------------+
| Field           | Type         | Null | Key | Default | Extra          |
+-----------------+--------------+------+-----+---------+----------------+
| id              | int(11)      | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)      | NO   | MUL | NULL    |                |
| title           | varchar(500) | YES  |     | NULL    |                |
| content         | varchar(500) | YES  |     | NULL    |                |
| datetime_posted | datetime     | YES  |     | NULL    |                |
+-----------------+--------------+------+-----+---------+----------------+
5 rows in set (0.01 sec)

mysql> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World", "2021-01-02 01:00:00");
Query OK, 1 row affected (0.00 sec)

mysql> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth", "2021-01-02 02:00:00");
Query OK, 1 row affected (0.00 sec)

mysql> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
Query OK, 1 row affected (0.00 sec)

mysql> INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye Solar System!", "2021-01-02 04:00:00");
Query OK, 1 row affected (0.00 sec)

mysql> SELECT * FROM posts;
+----+-----------+-------------+-----------------------+---------------------+
| id | author_id | title       | content               | datetime_posted     |
+----+-----------+-------------+-----------------------+---------------------+
|  1 |         1 | First Code  | Hello World           | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello Earth           | 2021-01-02 02:00:00 |
|  3 |         2 | Third Code  | Welcome to Mars!      | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye Solar System! | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------+---------------------+
4 rows in set (0.00 sec)

mysql> SELECT * FROM posts WHERE author_id = 1;
+----+-----------+-------------+-------------+---------------------+
| id | author_id | title       | content     | datetime_posted     |
+----+-----------+-------------+-------------+---------------------+
|  1 |         1 | First Code  | Hello World | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello Earth | 2021-01-02 02:00:00 |
+----+-----------+-------------+-------------+---------------------+
2 rows in set (0.01 sec)

mysql> SELECT email, datetime_created FROM users;
+-------------------------+---------------------+
| email                   | datetime_created    |
+-------------------------+---------------------+
| johnsmith@gmail.com     | 2021-01-01 01:00:00 |
| juandelecruz@gmail.com  | 2021-01-01 02:00:00 |
| janesmith@gmail.com     | 2021-01-01 03:00:00 |
| mariadelacruz@gmail.com | 2021-01-01 04:00:00 |
| johndoe@gmail.com       | 2021-01-01 05:00:00 |
+-------------------------+---------------------+
5 rows in set (0.00 sec)

mysql> UPDATE posts SET content = "Hello to the People of the Earth!" WHERE id = 2;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT * FROM posts;
+----+-----------+-------------+-----------------------------------+---------------------+
| id | author_id | title       | content                           | datetime_posted     |
+----+-----------+-------------+-----------------------------------+---------------------+
|  1 |         1 | First Code  | Hello World                       | 2021-01-02 01:00:00 |
|  2 |         1 | Second Code | Hello to the People of the Earth! | 2021-01-02 02:00:00 |
|  3 |         2 | Third Code  | Welcome to Mars!                  | 2021-01-02 03:00:00 |
|  4 |         4 | Fourth Code | Bye bye Solar System!             | 2021-01-02 04:00:00 |
+----+-----------+-------------+-----------------------------------+---------------------+
4 rows in set (0.00 sec)

mysql> DELETE FROM users WHERE email = "johndoe@gmail.com";
Query OK, 1 row affected (0.01 sec)

mysql> SELECT * FROM users;
+----+-------------------------+-----------+---------------------+
| id | email                   | password  | datetime_created    |
+----+-------------------------+-----------+---------------------+
|  1 | johnsmith@gmail.com     | passwordA | 2021-01-01 01:00:00 |
|  2 | juandelecruz@gmail.com  | passwordB | 2021-01-01 02:00:00 |
|  3 | janesmith@gmail.com     | passwordC | 2021-01-01 03:00:00 |
|  4 | mariadelacruz@gmail.com | passwordD | 2021-01-01 04:00:00 |
+----+-------------------------+-----------+---------------------+
4 rows in set (0.00 sec)

mysql>